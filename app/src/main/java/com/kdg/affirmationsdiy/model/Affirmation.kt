package com.kdg.affirmationsdiy.model

data class Affirmation(
    val stringResourceId: Int,
    val imageResourceId: Int
)
